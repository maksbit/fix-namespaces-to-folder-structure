﻿using System;
using System.IO;
using System.Linq;
using System.Text;

namespace FixNamespacesToFolderStructure
{
    class Program
    {
        private const string namespaceText = "namespace";

        static void Main(string[] args)
        {
            FixNamespaces(null);
        }

        private static void FixNamespaces(string dirPath)
        {
            while (dirPath == null)
            {
                Logger.Log("paste directory path with .sln or .csproj where to fix .cs files namespaces or Enter to exit");
                dirPath = Console.ReadLine();

                if (string.IsNullOrWhiteSpace(dirPath))
                    return;

                try
                {
                    var filesPaths = Directory.GetFiles(dirPath);
                    if (!filesPaths.Any(x => x.EndsWith(".sln") || x.EndsWith(".csproj")))
                    {
                        throw new Exception("could not find .sln or .csproj files in the target directory");
                    }
                }
                catch (Exception ex)
                {
                    Logger.Log("failed to get files from directory. Ex=" + ex.Message);
                }
            }

            var allFilesPaths = Directory.GetFiles(dirPath, "*.cs", SearchOption.AllDirectories);

            foreach (var path in allFilesPaths)
            {
                try
                {
                    var lines = File.ReadAllLines(path, Encoding.UTF8);

                    var namespaceLineIndex = -1;
                    for (int i = 0; i < lines.Length; i++)
                    {
                        if (lines[i].StartsWith(namespaceText))
                        {
                            namespaceLineIndex = i;
                            break;
                        }
                    }

                    if (namespaceLineIndex >= 0)
                    {
                        var fileNamespaceFullValue = lines[namespaceLineIndex];
                        var fileNamespaceValue = fileNamespaceFullValue.Split(' ')[1];

                        var fileDir = new DirectoryInfo(Path.GetDirectoryName(path));
                        while (fileDir.GetFiles("*.csproj", SearchOption.TopDirectoryOnly).Any() == false)
                        {
                            if (fileDir.Parent == null)
                            {
                                throw new Exception("failed to find .csproj file for file = " + path);
                            }
                            fileDir = fileDir.Parent;
                        }


                        var relativeFilePath = Path.GetDirectoryName(path).Substring(fileDir.Parent.FullName.Length + 1);//+1 for first \

                        var validNamespaceValue = relativeFilePath.Replace("\\", ".");

                        if (fileNamespaceValue != validNamespaceValue)
                        {
                            lines[namespaceLineIndex] = namespaceText + " " + validNamespaceValue;
                            File.WriteAllLines(path, lines, Encoding.UTF8);
                        }
                    }
                }
                catch (Exception ex)
                {
                    Logger.Log("failed to parse file =" + path
                        + Environment.NewLine
                        + " Ex=" + ex);
                    Console.ReadLine();
                    throw;
                }
            }

            Logger.Log("done");
            Console.ReadLine();
        }
    }
}
